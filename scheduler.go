package scheduler

import (
	"log"
	"time"
)

// Derived from scheduler in synth.go

// Event is an interface for ScheduledEvent objects
//
type Event interface {
	RenderEvent() // Render the event now
}

// Scheduler represents the scheduler
type Scheduler struct {
	schedule chan ScheduledEvent // events are sent via this channel for scheduling
	latency  uint                // Latency of rendering system in ms (informational metadata)
}

// ScheduledEvent represents an event that has been scheduled for rendering
//
type ScheduledEvent struct {
	deadline time.Time
	event    Event
	next     *ScheduledEvent
}

// NewScheduledEvent is a constructor for a ScheduledEvent
func NewScheduledEvent(event Event, delay time.Duration) *ScheduledEvent {
	deadline := time.Now().Add(delay * time.Millisecond)
	//log.Printf("Creating new event with deadline %s", deadline)
	sEvent := ScheduledEvent{deadline, event, nil}
	return &sEvent
}

// ScheduleEvent schedules an event to be rendered by sending the ScheduledEvent to the scheduler's event channel
func ScheduleEvent(s *Scheduler, ev *ScheduledEvent) {
	//log.Printf("Scheduling an event with deadline %s\n", ev.deadline)
	s.schedule <- *ev
}

// Init initialises Scheduler with all required parameters for use and starts it
func Init(latency uint) (*Scheduler, error) {
	log.Printf("Initialising Scheduler\n")
	scheduler := &Scheduler{
		schedule: make(chan ScheduledEvent),
		latency:  latency,
	}
	go scheduler.runScheduler()
	return scheduler, nil
}

// Main Scheduler loop.
func (s *Scheduler) runScheduler() {
	/*
	 *  This takes ScheduledEvent events and schedules them for execution
	 *  by running the Trigger method on them.
	 *  This method is generic and should be able to handle any sort of event
	 *  that is a Scheduled event.
	 */
	var pending *ScheduledEvent
	var timer *time.Timer
	var wake <-chan time.Time

	log.Printf("Running Scheduler\n")

	resched := func() {
		now := time.Now()
		d := time.Duration(0)
		// If pending event deadline is not after current time
		if !now.Before(pending.deadline) {
			d = pending.deadline.Sub(now) // Duration become negative
		} else {
			d = pending.deadline.Sub(now) // Calculate timer duration
		}
		// Set timer for duration until pending event
		//log.Printf(" Setting timer for %s", d)
		if timer == nil {
			timer = time.NewTimer(d)
			wake = timer.C
		} else {
			timer.Reset(d)
		}
	}
	for {
		select {
		// ScheduledEvents arrive via schedule channel
		case newEvent := <-s.schedule:
			// if pending == nil {
			// 	log.Printf(" Queue was empty\n")
			// 	pending = &newEvent
			// 	resched()
			// 	continue
			// } else if newEvent.deadline.Before(pending.deadline) {
			// 	newEvent.next = pending
			// 	pending = &newEvent
			// 	resched()
			// 	continue
			// }
			//log.Printf("Received event with deadline %s\n", newEvent.deadline)
			var node **ScheduledEvent
			for node = &pending; *node != nil && !newEvent.deadline.Before((*node).deadline); node = &((*node).next) {
			}
			newEvent.next = *node
			*node = &newEvent
			if node == &pending {
				resched()
			}
		case now := <-wake:
			timer, wake = nil, nil
			//log.Printf(" Waking up...\n")
			for pending != nil && !now.Before(pending.deadline) {
				//log.Printf(" Render event\n")
				pending.event.RenderEvent() // Trigger the renderer to immediately render the event
				//log.Printf(" Get next event\n")
				pending = pending.next // Get next event in queue
			}
			if pending != nil {
				//log.Printf(" Scheduling timer for next event\n")
				resched()
			}
		}
	}
}
