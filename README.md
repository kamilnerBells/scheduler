# Scheduler library

This provides a simple scheduler library that is designed primarily for use in the development of bell simulator applications.
This is required because, in such applications, the bell strike occurs hundreds of milliseconds after the sensor being triggered.

Thus, in such an application, there is the need to delay the simulated ring of the bell. This scheduler is designed to support that requirement.

## EventScheduler Description

This is a brief description of how the Event Scheduler works.

SchedulerInit initialises the Scheduler and starts it. It takes latency as an argument and this is used stored within the Scheduler struct. The Scheduler struct also contains the scheduler channel that is used to send events to the scheduler.

A ScheduleEvent method adds a scheduled event to the scheduler by sending the event to the scheduler's channel. The event sent to this channel is based on the ScheduledEvent struct which contains the event as well as the scheduled time and a link to the next event. This allows a queue of events to be built.

Within the ScheuldedEvent struct is the Event, which is an interface which defines the RenderEvent method used to render the event. This should allow mixed event types to be queued in the scheduler, each with their own renderer.

To use the scheduler, it should only be required to initialise it and then use the RenderEvent method to push things to it. The Event type needs to be implemented
